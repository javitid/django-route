# Servidor con threading# Servidor con threading
import socket
import threading
import SocketServer
import httplib
import errno

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        prev_time = 0
        prev_latitud = 0
        prev_longitud = 0
        while (True):
            data = self.request.recv(1024)
            while (data):
                cur_thread = threading.current_thread()
                response = cur_thread.name + ": " + data
                print response
                data_split = data.split(":")
                if (data_split[0] == "##,imei"): 
                    self.request.sendall("LOAD")
                    self.request.sendall("**,imei:" + data_split[1].split(",")[0]  + ",C,120s")
                elif (data_split[0] == "imei"):
                    data_string = data_split[1].split(",")
                    imei = data_string[0]
                    dispositivo = data_string[1]
                    timestamp = data_string[2]
                    fix_lost = data_string[4]
                    local_time = data_string[5]
                    latitud = data_string[7]
                    latitud_signo = data_string[8]
                    longitud = data_string[9]
                    longitud_signo = data_string[10]

                    if ((fix_lost == 'F') & (timestamp != 0)):
                        #lat_grados = latitud[0] + latitud[1]
                        #lat_minutos = latitud[2] + latitud[3] 
                        #lat_segundos = latitud[5] + latitud[6] + latitud[7] + latitud[8]
                        #lon_grados = longitud[0] + longitud[1] + longitud[2]
                        #lon_minutos = longitud[3] + longitud[4]
                        #lon_segundos = longitud[6] + longitud[7] + longitud[8] + longitud[9]

                        lat_grados = latitud[0:2]
                        lat_minutos = latitud[2:4] 
                        lat_segundos = latitud.split('.')[1]
                        lon_grados = 200
                        lon_minutos = 0
                        lon_segundos = 0
                        if (len(longitud.split('.')) == 2):
                            lon_grados = longitud[0:3]
                            lon_minutos = longitud[3:5]
                            lon_segundos = longitud.split('.')[1]
                        elif (len(longitud) == 1):
                            if (len(str(longitud[0]) == 5)):
                                lon_grados = longitud[0:3]
                                lon_minutos = longitud[3:5]
                        lat_bd = float(lat_grados) + float(lat_minutos)/60 + float(lat_segundos)/600000
                        lon_bd = float(lon_grados) + float(lon_minutos)/60 + float(lon_segundos)/600000
                        if (latitud_signo == 'S'):
                            lat_bd = -lat_bd
                        if (longitud_signo == 'W'):
                            lon_bd = -lon_bd
                        
                        #Introduce coordenadas en la base de datos, a no ser que las coordenadas se salgan de su rango o que estas difieran en mas de 1 grado a las recibidas anteriormente
                        if (((abs(lat_bd) <= 90) & (abs(lon_bd) <= 180)) & ((prev_latitud == 0) | ((abs(float(prev_latitud) - lat_bd) < 1) & (abs(float(prev_longitud) - lon_bd) < 1 )))):
                            try:
                                conn = httplib.HTTPConnection("localhost:8080")
                                #conn.request("GET", "/setCoords/?user=" + imei + "&x=" + str(lat_bd) + "&y=" + str(lon_bd) + "&z=0")
                                conn.request("GET", "/setCoords/?device=" + imei + "&lat=" + str(lat_bd) + "&lon=" + str(lon_bd))
                                res = conn.getresponse()
                                print res.status, res.reason
                                #print res.read()
                            except socket.error as e:
                                if e.errno == errno.ECONNREFUSED:
                                    print "Connection refused"
                            print "Latitud: " + str(lat_bd) + " ; Longitud: " + str(lon_bd)
                        else:
                            print "Coordenadas descartadas"

                        prev_time = (float(timestamp)*100)+float(local_time.split('.')[0][4:6])
                        if ((abs(lat_bd) <= 90) & (abs(lon_bd) <= 180)):
                            prev_latitud = lat_bd
                            prev_longitud = lon_bd

                elif (data[15] == ";"):
                    self.request.sendall("ON")
                data = ""

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


if __name__ == "__main__":
    HOST, PORT = "localhost", 4819

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = False
    server_thread.start()
    print "Server loop running in thread:", server_thread.name
