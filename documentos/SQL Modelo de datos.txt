BEGIN;
CREATE TABLE `coords_user` (
    `nick` varchar(20) NOT NULL PRIMARY KEY,
    `name` varchar(50) NOT NULL,
    `lastname` varchar(50) NOT NULL,
    `email` varchar(80) NOT NULL
)
;
CREATE TABLE `coords_device` (
    `imei` varchar(15) NOT NULL PRIMARY KEY,
    `type` varchar(50) NOT NULL,
    `access` integer NOT NULL,
    `user_id` varchar(20) NOT NULL
)
;
ALTER TABLE `coords_device` ADD CONSTRAINT `user_id_refs_nick_6568e840` FOREIGN KEY (`user_id`) REFERENCES `coords_user` (`nick`);
CREATE TABLE `coords_coords` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `device` varchar(20) NOT NULL,
    `lat` numeric(14, 10) NOT NULL,
    `lon` numeric(14, 10) NOT NULL,
    `alt` numeric(14, 10),
    `pub_date` datetime NOT NULL
)
;
COMMIT;