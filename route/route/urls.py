from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'route.views.home', name='home'),
    # url(r'^route/', include('route.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^coords/$', 'coords.views.get_coords'),
    url(r'^setCoords/$', 'coords.views.set_coords'),
    url(r'^showMap/$', 'coords.views.show_map'),
    #url(r'^coords/$', 'coords.views.post_coords'),
    #url(r'^form/$', 'coords.views.form'),
    #url(r'^test/$', 'coords.views.test'),
    url(r'^coordsJson/$', 'coords.views.get_coords_json'),
    url(r'^lastCoordJson/$', 'coords.views.get_last_coords_json'),
)
