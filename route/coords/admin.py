from django.contrib import admin
from coords.models import Device, User

admin.site.register(Device)
admin.site.register(User)