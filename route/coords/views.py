from django.shortcuts import render_to_response
from django.views.decorators.http import require_GET #,require_POST ,require_http_methods
from django.http import HttpResponse
#from django.template import RequestContext
#from django.template import Template, Context
#from django.template.loader import get_template
import datetime
import json
from coords.models import User, Coords, Device

#@require_http_methods(["GET", "POST"])
@require_GET
def get_coords(request):
    # http://host:port/coords?device=dispositivo&from=YYYY-MM-DD&to=YYYY_MM_DD
    now = datetime.datetime.now()
    # Selecciona en la base de datos el usuario pasado como parametro en la url
    device_str = request.GET.get('device')
    device = Device.objects.get(imei = device_str)   
    # Incrementa el numero de accesos a los datos del usuario
    device.access += 1
    device.save() 
    try:
        # Obtiene coordenadas 
        coords = raw_coords(request)
        # Muestra el resultado
        return render_to_response('show_coords.html', locals())
    except:
        return render_to_response('no_user_in_db.html', locals())

@require_GET
def get_coords_json(request):
    # http://host:port/coordsJson?device=dispositivo&from=YYYY-MM-DD&to=YYYY_MM_DD
    response_data = {}
    coords_json = []
    # Obtiene coordenadas 
    coords = raw_coords(request)
    # Rellena el resultado
    for coord in coords:
        coords_json.append([unicode(coord.pub_date), str(coord.lat), str(coord.lon), str(coord.alt)])
    # Muestra el resultado
    response_data['coords'] = coords_json
    return HttpResponse(json.dumps(response_data), mimetype="application/json")

@require_GET
def get_last_coords_json(request):
    response_data = {}
    coords_json = []
    # Recoge el usuario pasado en la url
    device_str = request.GET.get('device')
    # Obtiene las ultimas coordenadas del usuario
    coords = Coords.objects.filter(device = device_str).order_by('-pub_date')[:1]
    for coord in coords:
        coords_json.append([unicode(coord.pub_date), str(coord.lat), str(coord.lon), str(coord.alt)])
    # Muestra el resultado
    response_data['coords'] = coords_json
    return HttpResponse(json.dumps(response_data), mimetype="application/json")

@require_GET
def show_map(request):
    # Ejemplo: http://localhost:8000/showMap?device=javi
    # Recoge el usuario pasado en la url
    device_str = request.GET.get('device')
    # Obtiene las ultimas coordenadas del usuario
    coords = Coords.objects.filter(device = device_str).order_by('-pub_date')[:1]
    lat = ''
    lon = ''
    for coord in coords:
        lat = str(coord.lat).replace(',','.')
        lon = str(coord.lon).replace(',','.')
    # Muestra el resultado
    if (lat != ''):
        return render_to_response('show_map.html', {'lat':lat, 'lon':lon})
    else:
        return render_to_response('no_user_in_db.html', {'device_str':device_str})
    
@require_GET
def set_coords(request):
    # Ejemplo: http://localhost:8000/setCoords?device=javi&lat=1.2323&lon=2.4431&alt=3.2344
    # Coge la fecha y hora actual
    now = unicode(datetime.datetime.now())
    # Recoge el usuario y las coordenadas pasadas en la url
    device_str = request.GET.get('device')
    lat = request.GET.get('lat', '')
    lon = request.GET.get('lon', '')
    alt = request.GET.get('alt', '')

    # Comprueba que se han pasado al menos el usuario, la latitud y longitud como parametros
    if (device_str and lat and lon):
        if (alt):
            coords = Coords.objects.create(device=device_str, lat=lat, lon=lon, alt=alt, pub_date=now)
        else:
            coords = Coords.objects.create(device=device_str, lat=lat, lon=lon, pub_date=now)
        return render_to_response('insert_coords.html', locals())
    else:
        return render_to_response('no_coords.html', locals())


def raw_coords(request):
    # Recoge datos pasados en la url
    device_str = request.GET.get('device')
    from_str = request.GET.get('from', '')
    to_str = request.GET.get('to', '')
    coords = ''
    # Obtiene las coordenadas del usuario entre un rango de fechas (from, to)
    # Modificadores: lte=antes de YYYY-MM-DD; gte=despues de YYYY-MM-DD;
    if (from_str or to_str):
        if (not from_str):
            coords = Coords.objects.filter(device = device_str, pub_date__lte=to_str).order_by('-pub_date')
        elif (not to_str):
            coords = Coords.objects.filter(device = device_str, pub_date__gte=from_str).order_by('-pub_date')
        else:
            coords = Coords.objects.filter(device = device_str, pub_date__gte=from_str, pub_date__lte=to_str).order_by('-pub_date')
    # Obtiene todas las coordenadas del usuario
    else:
        coords = Coords.objects.filter(device = device_str).order_by('-pub_date')
    return coords

"""
@require_POST
def post_coords(request):
    #meta = request.META.items()
    print "POSTED"
    lat = request.GET.get('lat', '')
    lon = request.GET.get('lon', '')
    alt = request.GET.get('alt', '')
    
    csrfContext = RequestContext(request, locals())
    return render_to_response('insert_coords.html', csrfContext)

@require_GET
def form(request):
    return render_to_response('post_coords.html', context_instance=RequestContext(request))

@require_GET
def test(request):
    #now = datetime.datetime.now()
    #template = get_template('show_coords.html')
    #context = Context({'now': now})
    #return HttpResponse(template.render(context))
    #return render_to_response('show_coords.html', {'now': now})
    users = User.objects.all()
    return HttpResponse(users)
"""

