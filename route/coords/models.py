from django.db import models

# Create your models here.
class User(models.Model):
    nick = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    email = models.EmailField(max_length=80)

    def __str__(self):
        return self.nick

class Device(models.Model):
    imei = models.CharField(max_length=15, primary_key=True)
    type = models.CharField(max_length=50)
    access = models.IntegerField()
    user = models.ForeignKey(User)

    def __str__(self):
        return self.imei

class Coords(models.Model):
    device = models.CharField(max_length=20)
    lat = models.DecimalField(max_digits=14, decimal_places=10)
    lon = models.DecimalField(max_digits=14, decimal_places=10)
    alt = models.DecimalField(max_digits=14, decimal_places=10, null=True)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.pub_date
    
    def __unicode__(self):
        return unicode(self.pub_date)
    
